#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <stdlib.h>
#include <limits.h>
#include <nbst/nbst_lib.h>

const char *prog;

enum {
  MAX_SEARCH_ORDER = 1024
};

static struct timespec
time_elapsed(
    const struct timespec start,
    const struct timespec end) {

  struct timespec ret;

  ret.tv_sec = end.tv_sec - start.tv_sec;
  if ( end.tv_nsec >= start.tv_nsec )
    ret.tv_nsec = end.tv_nsec - start.tv_nsec;
  else {
    --ret.tv_nsec;
    ret.tv_nsec = 1000000000L + end.tv_nsec - start.tv_nsec;
  }

  return ret;
}

static void
usage() {
  printf(
      "usage: %s [-s <test set size>] [-o <tree order>] [-h]\n",
      prog);
}

#define try(cond) if (nbst_unlikely(cond)) goto err
#define nbst_try(cmd) try((cmd) != NBST_OK)

static int
fill(
    nbst_long_t *tree,
    size_t size) {

  __label__ end, err;
  int ret = 0;
  size_t i;

  for(i = 0; i < size; ++i)
    nbst_try(nbst_long_insert(tree, i));

end:
  return ret;

err:
  ret = 1;
  goto end;
}

static int
search(
    nbst_long_t *tree,
    size_t size) {

  __label__ end, err;
  int ret = 0;
  size_t i;
  long k;

  for(i = 0; i < size; ++i) {

    nbst_try(nbst_long_search(tree, i, &k));
    try(i != k);

  }

end:
  return ret;

err:
  ret = 1;
  goto end;
}

static int
tree_remove(
    nbst_long_t *tree,
    size_t size) {

  __label__ end, err;
  int ret = 0;
  size_t i;

  for(i = 0; i < size; ++i)
    nbst_try(nbst_long_remove(tree, i));

end:
  return ret;

err:
  ret = 1;
  goto end;

}

static int
do_bench(
    size_t size,
    size_t order) {

  __label__ end, err;
  int ret = 0;
  nbst_long_t *tree;

  nbst_try(nbst_long_new(&tree, order));

  try(fill(tree, size));
  try(search(tree, size));
  try(tree_remove(tree, size));

  nbst_long_free(tree);

end:
  return ret;

err:
  ret = 1;
  goto end;

}

static int
test_onetime(
    size_t size,
    size_t order) {

  __label__ end, err;

  int ret = 0;
  struct timespec start_time, end_time, diff;

  if ( order == 0 )
    order = 3;

  clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start_time);

  try(do_bench(size, order));

  clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &end_time);

  diff = time_elapsed(start_time, end_time);

  printf(
      "size: %zd order: %zd time: %ld.%09ld\n",
      size, order, diff.tv_sec, diff.tv_nsec);

end:
  return ret;

err:
  ret = 1;
  goto end;
}

static int
search_fastes(
    size_t size) {

  __label__ end, err;
  int ret = 0;
  size_t right = 3;
  struct timespec
      start_time, end_time, cur_time;

  printf("benchmark\n");
  for(right = 3; right < 256; right += 2) {

    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start_time);

    try(do_bench(size, right));

    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &end_time);

    cur_time = time_elapsed(start_time, end_time);

    printf(
        "order: %3zd time: %ld.%09ld\n",
        right, cur_time.tv_sec, cur_time.tv_nsec);

  }

end:
  return ret;

err:
  ret = 1;
  goto end;

}

int
main(
    int argc,
    char **argv) {

  __label__ end, err;

  prog = argv[0];
  size_t size = 32 * 1024 * 1024;
  size_t order = 0;
  int ret = 0;
  int c;
  struct timespec t;

  while((c = getopt(argc, argv, "s:o:h")) != -1) {
    switch(c) {

      case 's':
        size = atol(optarg);
        break;

      case 'o':
        order = atol(optarg);
        break;

      case 'h':
        usage();
        goto end;

      case '?':
        usage();
        goto err;

    }
  }

  try(clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &t));

  if ( order != 0 ) {
    try(test_onetime(size, order));
  } else {
    try(search_fastes(size));
  }

end:
  return ret;

err:
  printf("benchmark failed\n");
  ret = 1;
  goto end;
}
