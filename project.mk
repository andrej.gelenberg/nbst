PROJECT_NAME := libnbst
AUTHOR := Andrej Gelenberg <andrej.gelenberg@udo.edu>
VER_MAJOR := 0
VER_MINOR := 6

all: libnbst.so

libnbst.so: libnbst.o
libnbst.so: LIBS=

tests/test bench: libnbst.so.$(VER_MAJOR)
tests/test bench: LIBS=-lnbst
tests/test bench: LDFLAGS=-L.
tests/test: tests/test.o

bench: bench.o

CFLAGS += -I.

CLEAN += tests/test bench

tests/test bench libnbst.so: Makefile

.PHONY: test do_bench
test: tests/test
	LD_LIBRARY_PATH=. ./tests/test

ntest: tests/ntest
	LD_LIBRARY_PATH=. ./tests/ntest

valgrind: tests/test
	LD_LIBRARY_PATH=. valgrind ./tests/test

gdb: tests/test
	LD_LIBRARY_PATH=. gdb ./tests/test

do_bench:
	./bench

PERF ?= perf
perf: tests/test
	LD_LIBRARY_PATH=. $(PERF) record ./tests/test

$(eval $(call install_so,libnbst.so))
install: $(foreach i,$(shell find nbst/ -name '*.h'), $(DESTDIR)/usr/include/$i)
