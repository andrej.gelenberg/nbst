/* Copyright (c) 2013-2015 Andrej Gelenberg <andrej.gelenberg@udo.edu> */

#include <string.h>
#include <stdint.h>

#define NBST_LIB_C
#include <nbst/nbst_lib.h>

#define KEY_T long
#include <nbst/nbst_impl.h>

#define KEY_T long
#define VALUE_T ptr
#include <nbst/nbst_impl.h>

#define KEY_T double
#include <nbst/nbst_impl.h>


#define KEY_T hcstr
#define KEY_CMP
#define KEY_COPY
#define KEY_FREE
#include <nbst/nbst_impl.h>

#define VALUE_T cstr
#define VALUE_COPY
#define VALUE_FREE
#include <nbst/nbst_strkey.h>

#define VALUE_T ptr
#include <nbst/nbst_strkey.h>

#ifdef DEBUG
#  ifndef nbst_debug
#    include <stdio.h>
#    define nbst_debug(format, ...) \
         fprintf(stderr, "debug: %s: " format "\n", __func__, ## __VA_ARGS__); \
         fflush(stderr)
#  endif /* nbst_debug */
#else
# define nbst_debug(format, ...)
#endif /* DEBUG */

struct nbst_hcstr_foreach_data_t {
  nbst_cstr_foreach_cb_f *cb;
  void                   *data;
};
typedef struct nbst_hcstr_foreach_data_t nbst_hcstr_foreach_data_t;

int
hcstr_cmp(
    hcstr a,
    hcstr b) {

  int ret;

  if ( nbst_unlikely(a.hash == b.hash) )
    ret = strcmp(a.str, b.str);
  else
    ret = a.hash > b.hash ? 1 : -1;

  return ret;

}

nbst_ret_t
hcstr_copy(
          hcstr *d,
    const hcstr  s) {

  nbst_ret_t ret;
  d->hash = s.hash;
  d->str  = strdup(s.str);

  if ( nbst_unlikely(d->str == 0) )
    ret = NBST_NOMEM;
  else
    ret = NBST_OK;

  return ret;

}

uint64_t
hstr_hash(
    const char *str) {

  const char *i;
  uint64_t ret = 14695981039346656037ULL;

  for(i = str; *i; ++i) {
    ret ^= *i;
    ret *= 1099511628211ULL;
  }

  return ret;

}

void
hcstr_init(
          hcstr *t,
    const char  *str) {

  t->hash = hstr_hash(str);
  t->str  = str;

}

const char *
nbst_error_message(
    nbst_ret_t err) {

  const char *ret;
  static const char * const msgs[] = {
      [NBST_OK]               = "everything is ok",
      [NBST_NOMEM]            = "not enough memory",
      [NBST_OVERFLOW]         = "requested tree order is too big",
      [NBST_EXISTS]           = "key alredy in the tree",
      [NBST_NOTFOUND]         = "key is not in the tree",
      [NBST_FOREACH_CB_ERROR] = "foreach callback returned error",

      [NBST_TODO]             = "some thing is still to do in implementaion",
    };

  if ( err > NBST_TODO ) {
    ret = "invalid error code";
  } else {
    ret = msgs[err];
  }

  return ret;

}

nbst_ret_t
nbst_cstr_new(
          nbst_cstr_t ** const tree,
    const size_t               order) {

  return nbst_hcstr_new((nbst_hcstr_t**)tree, order);

}

nbst_ret_t
nbst_cstr_init(
          nbst_cstr_t * const tree,
    const size_t              order) {

    return nbst_hcstr_init((nbst_hcstr_t*)tree, order);

}

void
nbst_cstr_free(
    nbst_cstr_t * const tree) {

  nbst_hcstr_free((nbst_hcstr_t*)tree);

}

void
nbst_cstr_destroy(
    nbst_cstr_t * const tree) {

  nbst_hcstr_destroy((nbst_hcstr_t*)tree);

}

nbst_ret_t
nbst_cstr_insert(
          nbst_cstr_t * const tree,
    const cstr                key) {

  hcstr ik;
  hcstr_init(&ik, key);

  return
      nbst_hcstr_insert(
          (nbst_hcstr_t*)tree,
          ik);

}

nbst_ret_t
nbst_cstr_search(
    const nbst_cstr_t * const tree,
    const cstr          const key,
          cstr *        const retkey) {

  hcstr ik;
  hcstr_init(&ik, key);

  nbst_ret_t ret =
      nbst_hcstr_search(
          (const nbst_hcstr_t *)tree,
          ik,
          &ik);

  if ( nbst_likely(retkey && (ret == NBST_OK)) ) {
    *retkey = ik.str;
  }

  return ret;

}

nbst_ret_t
nbst_cstr_remove(
          nbst_cstr_t * const tree,
    const cstr                key) {

  hcstr ik;
  hcstr_init(&ik, key);

  return
      nbst_hcstr_remove(
          (nbst_hcstr_t*)tree,
          ik);

}

nbst_foreach_ret_t
nbst_cstr_foreach_handler(
    const hcstr  key,
          void  *cb_data) {

  nbst_hcstr_foreach_data_t *data = cb_data;
  return data->cb(key.str, data->data);

}

nbst_ret_t
nbst_cstr_foreach(
    const nbst_cstr_t            * const tree,
          void                   * const data,
          nbst_cstr_foreach_cb_f * const cb) {

  nbst_hcstr_foreach_data_t d = { cb, data };

  return
      nbst_hcstr_foreach(
          (const nbst_hcstr_t*)tree,
          &d,
          nbst_cstr_foreach_handler);

}

void
hcstr_free(
    hcstr key) {

  free((char *)key.str);

}

static nbst_ret_t
nbst_cstr_cstr_value_copy(
          cstr *dst,
    const cstr  src) {

  nbst_ret_t ret;
  *dst = strdup(src);

  nbst_debug("%p -> %p", src, *dst);

  if ( nbst_unlikely(*dst == 0) )
    ret = NBST_NOMEM;
  else
    ret = NBST_OK;

  return ret;

}

static void
nbst_cstr_cstr_value_free(
    cstr v) {

  free((char *)v);

}

size_t
nbst_cstr_length(
    const nbst_cstr_t * const tree) {

  return nbst_hcstr_length((nbst_hcstr_t *)tree);

}

static int
nbst_hcstr_key_cmp(
    hcstr a,
    hcstr b) {

  return hcstr_cmp(a, b);

}

static nbst_ret_t
nbst_hcstr_key_copy(
          hcstr *d,
    const hcstr  s) {

  return hcstr_copy(d, s);

}

static void
nbst_hcstr_key_free(
    hcstr key) {

  hcstr_free(key);

}
