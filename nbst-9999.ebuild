# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

DESCRIPTION="Generic b-trees implementation in C for in memory data management."
HOMEPAGE="https://bitbucket.org/nikel123/nbst"
EGIT_REPO_URI="https://bitbucket.org/nikel123/nbst.git"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~x86 ~amd64"

inherit git-r3
