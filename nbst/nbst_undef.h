/* Copyright (c) 2013-2014 Andrej Gelenberg <andrej.gelenberg@udo.edu> */

/* =undef= */
/* =data types= */
#undef nbst_t
#undef nbst_node_t

/* =functions= */
/* ===data manipulation=== */
#undef nbst_insert
#undef nbst_set
#undef nbst_insertset
#undef nbst_remove

/* ===data retrive=== */
#undef nbst_search
#undef nbst_foreach
#undef nbst_foreach_cb_f

/* ===meta functions=== */
#undef nbst_length

/* ===administrative functions=== */
#undef nbst_new
#undef nbst_init
#undef nbst_free
#undef nbst_destroy
#undef nbst_wr_lock
#undef nbst_rd_lock
#undef nbst_unlock

/* ===helper functions=== */
#undef nbst_key_cmp
#undef nbst_key_copy
#undef nbst_key_free

#undef nbst_value_copy
#undef nbst_value_free

#undef nbst_max_order

#undef nbst_node_insert
#undef nbst_node_find_key
#undef nbst_node_childs
#ifdef VALUE_T
#undef nbst_node_vals
#endif /* VALUE_T */
#undef nbst_node_merge
#undef nbst_node_free
#undef nbst_node_size
#undef nbst_node_split
#undef nbst_node_rebalance_left_to_right
#undef nbst_node_rebalance_right_to_left

#undef nbst_mid_node_new
#undef nbst_mid_node_free
#undef nbst_mid_node_inject
#undef nbst_mid_node_size
#undef nbst_mid_node_split
#undef nbst_mid_node_remove
#undef nbst_mid_node_foreach

#undef nbst_end_node_new
#undef nbst_end_node_size
#undef nbst_end_node_inject
#undef nbst_end_node_split
#undef nbst_end_node_remove
#undef nbst_end_node_foreach

#undef nbst_root_node_split

/* ==helper macros== */
#undef nbst_func
#undef NBST_STATIC
#undef KEY_T
#undef KEY_CMP
#undef KEY_FREE
#undef KEY_COPY

#undef VALUE_T
#undef VALUE_FREE
#undef VALUE_COPY

#undef NBST_DONT_UNDEF

#undef nbst_name
#undef _nbst_name
#undef __nbst_name

#undef _NBST_IMPL_H
