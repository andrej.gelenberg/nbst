/* Copyright (c) 2013-2014 Andrej Gelenberg <andrej.gelenberg@udo.edu> */

#ifndef NBST_COMMON_H
#define NBST_COMMON_H

#include <stddef.h>

enum nbst_ret_t {
  NBST_OK               =  0,
  NBST_NOMEM            =  1,
  NBST_OVERFLOW         =  2,
  NBST_EXISTS           =  3,
  NBST_NOTFOUND         =  4,
  NBST_FOREACH_CB_ERROR =  5,

  NBST_TODO
};
typedef enum nbst_ret_t nbst_ret_t;

enum nbst_foreach_ret_t {
  NBST_FOREACH_OK      = 0,
  NBST_FOREACH_STOP_OK = 1,
  NBST_FOREACH_ERROR
};
typedef enum nbst_foreach_ret_t nbst_foreach_ret_t;

#define nbst_likely(x)   __builtin_expect((x),1)
#define nbst_unlikely(x) __builtin_expect((x),0)

#endif /* NBST_COMMON_H */
