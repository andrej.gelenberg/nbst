/* Copyright (c) 2013-2015 Andrej Gelenberg <andrej.gelenberg@udo.edu> */
#ifndef VALUE_T
# error "VALUE_T is undefined"
#endif

#ifndef NBST_LIB_H
# error "you have to include <nbst/nbst_lib.h> prior using nbst/nbst_strkey.h"
#endif /* NBST_LIB_H */

#ifdef NBST_STATIC
# define _NBST_STATIC
#else
# define NBST_STATIC
#endif

#define NBST_DONT_UNDEF

#define KEY_T hcstr
#define KEY_COPY
#define KEY_CMP
#define KEY_FREE
#include <nbst/nbst_impl.h>

static int
nbst_key_cmp(
    KEY_T a,
    KEY_T b
  ) {

  return hcstr_cmp(a, b);

}

static nbst_ret_t
nbst_key_copy(
          KEY_T *d,
    const KEY_T  s) {

  return hcstr_copy(d, s);

}

static void
nbst_key_free(
    KEY_T k) {

  hcstr_free(k);

}


#ifdef VALUE_COPY

#define _nbst_value_copy _nbst_name(cstr, VALUE_T, value_copy)
static nbst_ret_t
_nbst_value_copy(
          VALUE_T *dst,
    const VALUE_T  src);

static nbst_ret_t
nbst_value_copy(
          VALUE_T *dst,
    const VALUE_T  src) {

  return _nbst_value_copy(dst, src);

}

#undef _nbst_value_copy

#endif /* VALUE_COPY */

#ifdef VALUE_FREE

#define _nbst_value_free _nbst_name(cstr, VALUE_T, value_free)
static void
_nbst_value_free(
    VALUE_T v);

static void
nbst_value_free(
    VALUE_T v) {

  _nbst_value_free(v);

}

#undef _nbst_value_free

#endif /* VALUE_FREE */

#ifndef _NBST_STATIC
#undef NBST_STATIC
#endif /* _NBST_STATIC */
#undef _NBST_STATIC

#undef nbst_func

#define NBST_DONT_UNDEF
#undef KEY_T
#define KEY_T cstr
#include <nbst/nbst_api.h>

#define _nbst_t _nbst_name(hcstr, VALUE_T, t)
#define _nbst_f(name, ...) _nbst_name(hcstr, VALUE_T, name)(__VA_ARGS__)
#define _nbst_ft(name, tree, ...) _nbst_f(name, (_nbst_t *)tree, ## __VA_ARGS__)

nbst_func nbst_ret_t
nbst_new(
    nbst_t ** const tree,
    const size_t order) {

  return _nbst_f(new, (_nbst_t **)tree, order);

}

nbst_func void
nbst_free(
    nbst_t * const tree) {

  return _nbst_ft(free, tree);

}

nbst_func nbst_ret_t
nbst_insert(
          nbst_t * const tree,
    const KEY_T          key,
    const VALUE_T        value
  ) {

  hcstr hkey;
  hcstr_init(&hkey, key);

  return _nbst_ft(insert, tree, hkey, value);

}

nbst_func nbst_ret_t
nbst_set(
          nbst_t * const tree,
    const KEY_T          key,
    const VALUE_T        value) {

  hcstr hkey;
  hcstr_init(&hkey, key);

  return _nbst_ft(set, tree, hkey, value);

}

nbst_func nbst_ret_t
nbst_remove(
          nbst_t * const tree,
    const KEY_T          key) {

  hcstr hkey;
  hcstr_init(&hkey, key);

  return _nbst_ft(remove, tree, hkey);

}

nbst_func nbst_ret_t
nbst_search(
    const nbst_t * const tree,
    const KEY_T          key,
          KEY_T *        key_ret,
          VALUE_T *      value) {

  hcstr hkey;
  hcstr_init(&hkey, key);

  nbst_ret_t ret = _nbst_ft(search, tree, hkey, &hkey, value);

  if ( key_ret != 0 )
    *key_ret = hkey.str;

  return ret;

}

nbst_func size_t
nbst_length(
    const nbst_t * const tree) {

  return _nbst_ft(length, tree);

}

#define nbst_foreach_handler   nbst_name(foreach_handler)
#define nbst_foreach_handler_t nbst_name(foreach_handler_t)

struct nbst_foreach_handler_t {
  void              *cb_data;
  nbst_foreach_cb_f *cb;
};
typedef struct nbst_foreach_handler_t nbst_foreach_handler_t;

static nbst_foreach_ret_t
nbst_foreach_handler(
    const hcstr    hkey,
          VALUE_T  value,
          void    *cb_data) {

  nbst_foreach_handler_t *data = cb_data;
  return data->cb(hkey.str, value, data->cb_data);

}

nbst_func nbst_ret_t
nbst_foreach(
    const nbst_t            * const tree,
          void              * const cb_data,
          nbst_foreach_cb_f * const cb) {

  nbst_foreach_handler_t data = { cb_data, cb};
  return _nbst_ft(foreach, tree, &data, nbst_name(foreach_handler));

}

#undef nbst_foreach_handler
#undef nbst_foreach_handler_t
#undef _nbst_t
#undef _nbst_f
#undef _nbst_ft

#include <nbst/nbst_undef.h>
