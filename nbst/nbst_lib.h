/* Copyright (c) 2013-2015 Andrej Gelenberg <andrej.gelenberg@udo.edu> */

#ifndef NBST_LIB_H
#define NBST_LIB_H

#include <stdint.h>
#include <nbst/nbst_common.h>

struct hcstr {
        uint64_t  hash;
  const char     *str;
};

typedef struct hcstr hcstr;
typedef const char * cstr;
typedef void *       ptr;

void
hcstr_init(
         hcstr *t,
   const char  *str);

const char *
nbst_error_message(
    nbst_ret_t err);

int
hcstr_cmp(
    hcstr a,
    hcstr b);

nbst_ret_t
hcstr_copy(
          hcstr *d,
    const hcstr  s);

void
hcstr_free(
    hcstr k);

#define KEY_T cstr
#include <nbst/nbst_api.h>

#define KEY_T   cstr
#define VALUE_T cstr
#include <nbst/nbst_api.h>

#define KEY_T   cstr
#define VALUE_T ptr
#include <nbst/nbst_api.h>

#ifndef NBST_LIB_C

#  define KEY_T long
#  include <nbst/nbst_api.h>

#  define KEY_T  long
#  define VALUE_T cstr
#  include <nbst/nbst_api.h>

#  define KEY_T long
#  define VALUE_T ptr
#  include <nbst/nbst_api.h>

#  define KEY_T double
#  include <nbst/nbst_api.h>

#  define KEY_T hcstr
#  include <nbst/nbst_api.h>

#  define KEY_T   hcstr
#  define VALUE_T cstr
#  include <nbst/nbst_api.h>

#  define KEY_T   hcstr
#  define VALUE_T ptr
#  include <nbst/nbst_api.h>

#endif /* NBST_LIB_C */

#endif /* NBST_LIB_H */
