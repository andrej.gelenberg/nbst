/* Copyright (c) 2013-2014 Andrej Gelenberg <andrej.gelenberg@udo.edu> */

#ifndef KEY_T
# error you must define KEY_T before use nbst_api.h
#endif

#include <nbst/nbst_common.h>

#ifdef VALUE_T
# define __nbst_name(key_type, data_type, suffix) nbst_ ## key_type ## _ ## data_type ## _ ##suffix
# define _nbst_name(key_type, data_type, suffix)  __nbst_name(key_type, data_type, suffix)
# define nbst_name(suffix)                        _nbst_name(KEY_T, VALUE_T, suffix)
#else /* VALUE_T */
# define __nbst_name(key_type, suffix) nbst_ ## key_type ## _ ## suffix
# define _nbst_name(key_type, suffix)  __nbst_name(key_type, suffix)
# define nbst_name(suffix)             _nbst_name(KEY_T, suffix)
#endif /* VALUE_T */

/* =Naming helper macros= */
/* ==data type names== */
#define nbst_t          nbst_name(t)
#define nbst_node_t     nbst_name(node_t)
#define nbst_tuple_t    nbst_name(tuple_t)

/* ==function names== */
/* ===data manipulation=== */
#define nbst_insert    nbst_name(insert)
#define nbst_set       nbst_name(set)
#define nbst_remove    nbst_name(remove)

/* ===data retrival=== */
#define nbst_search         nbst_name(search)
#define nbst_foreach        nbst_name(foreach)
#define nbst_foreach_cb_f   nbst_name(foreach_cb_f)

/* ===meta functions=== */
#define nbst_length    nbst_name(length)

/* ===administrative function names=== */
#define nbst_new       nbst_name(new)
#define nbst_free      nbst_name(free)

#ifdef NBST_STATIC
# define nbst_func static
#else
# define nbst_func
#endif

struct nbst_t;
typedef struct nbst_t nbst_t;

/* =function definition */
/* ==administrative functions== */
nbst_func nbst_ret_t
nbst_new(
          nbst_t ** const tree,
    const size_t          order)
  __attribute__((warn_unused_result));

nbst_func void
nbst_free(
    nbst_t * const tree);

/* ====data manipulation==== */
nbst_func nbst_ret_t
nbst_insert(
          nbst_t * const tree,
    const KEY_T          key
#ifdef VALUE_T
   ,const VALUE_T        value
#endif /* VALUE_T */
  )  __attribute__((warn_unused_result));

#ifdef VALUE_T
nbst_func nbst_ret_t
nbst_set(
          nbst_t * const tree,
    const KEY_T          key,
    const VALUE_T        value)
  __attribute__((warn_unused_result));
#endif /* VALUE_T */

nbst_func nbst_ret_t
nbst_remove(
          nbst_t * const tree,
    const KEY_T          key)
  __attribute__((warn_unused_result));

/* ====data retrive==== */
nbst_func nbst_ret_t
nbst_search(
    const nbst_t *  const tree,
    const KEY_T           key,
          KEY_T *   const key_ret
#ifdef VALUE_T
   ,      VALUE_T * const value
#endif /* VALUE_T */
  ) __attribute__((warn_unused_result));

typedef nbst_foreach_ret_t
    (nbst_foreach_cb_f)(
        const KEY_T    key,
#ifdef VALUE_T
              VALUE_T  value,
#endif /* VALUE_T */
              void    *callback_data
      );

nbst_func nbst_ret_t
nbst_foreach(
    const nbst_t            * const tree,
          void              * const cb_data,
          nbst_foreach_cb_f * const cb);

/* ====meta functions==== */
nbst_func size_t
nbst_length(
    const nbst_t * const tree)
  __attribute__((warn_unused_result));

#ifndef NBST_DONT_UNDEF
# ifndef _NBST_IMPL_H
#   include <nbst/nbst_undef.h>
# endif /* _NBST_IMPL_H */
#endif /* NBST_DONT_UNDEF */
