/* Copyright (c) 2013-2014 Andrej Gelenberg <andrej.gelenberg@udo.edu> */

#ifndef _NBST_IMPL_H_ONETIME
#define _NBST_IMPL_H_ONETIME
# include <stdlib.h>
# include <stdint.h>
# include <string.h>
# include <alloca.h>
# include <errno.h>
#endif /* _NBST_IMPL_H_ONETIME */

#define _NBST_IMPL_H
#include <nbst/nbst_api.h>

#ifndef log_warning
# define log_warning(msg, ...)
#endif /* log_warning */

#ifndef VALUE_T
# ifdef VALUE_FREE
#   error "VALUE_FREE defined but VALUE_T is not"
# endif /* VALUE_FREE */
# ifdef VALUE_COPY
#   error "VALUE_COPY defined but VALUE_T is not"
# endif /* VALUE_COPY */
#endif /* VALUE_T */

struct nbst_node_t {
   size_t      fill;
   KEY_T       keys[];
/* VALUE_T      values[order]; ifdef VALUE_T  */
/* nbst_node_t childs[order]; if not end node */
};
typedef struct nbst_node_t nbst_node_t;

struct nbst_t {
  size_t order;
  size_t length;
  size_t level;
  nbst_node_t *root;
};

/* =internal implementation helper macros= */
/* ==helper function names== */
#define nbst_init      nbst_name(init)
#define nbst_destroy   nbst_name(destroy)

#define nbst_key_cmp                      nbst_name(key_cmp)
#define nbst_key_copy                     nbst_name(key_copy)
#define nbst_key_free                     nbst_name(key_free)

#define nbst_value_copy                   nbst_name(value_copy)
#define nbst_value_free                   nbst_name(value_free)

#define nbst_max_order                    nbst_name(max_order)

#define nbst_insertset                    nbst_name(insertset)
#define nbst_node_insert                  nbst_name(node_insert)
#define nbst_node_find_key                nbst_name(node_find_key)
#define nbst_node_childs                  nbst_name(node_childs)
#ifdef VALUE_T
#define nbst_node_values                    nbst_name(node_vals)
#endif /* VALUE_T */
#define nbst_node_merge                   nbst_name(node_merge)
#define nbst_node_free                    nbst_name(node_free)
#define nbst_node_size                    nbst_name(node_size)
#define nbst_node_split                   nbst_name(node_split)
#define nbst_node_rebalance_left_to_right nbst_name(node_rebalance_left_to_right)
#define nbst_node_rebalance_right_to_left nbst_name(node_rebalance_right_to_left)

#define nbst_mid_node_new                 nbst_name(mid_node_new)
#define nbst_mid_node_free                nbst_name(mid_node_free)
#define nbst_mid_node_inject              nbst_name(mit_node_inject)
#define nbst_mid_node_size                nbst_name(mid_node_size)
#define nbst_mid_node_split               nbst_name(mid_node_split)
#define nbst_mid_node_remove              nbst_name(mid_node_remove)
#define nbst_mid_node_foreach             nbst_name(mid_node_foreach)

#define nbst_end_node_new                 nbst_name(end_node_new)
#define nbst_end_node_size                nbst_name(end_node_size)
#define nbst_end_node_inject              nbst_name(end_node_inject)
#define nbst_end_node_split               nbst_name(end_node_split)
#define nbst_end_node_remove              nbst_name(end_node_remove)
#define nbst_end_node_foreach             nbst_name(end_node_foreach)

#define nbst_root_node_split              nbst_name(root_node_split)

/* ==helper macros== */
#define nbst_midend(level, func_name, ...) \
  if ( nbst_likely(level) ) { \
    nbst_name( mid_ ## func_name ) ( __VA_ARGS__ ); \
  } else { \
    nbst_name( end_ ## func_name ) ( __VA_ARGS__ ); \
  }

#ifdef DEBUG
#  ifndef nbst_debug
#    include <stdio.h>
#    define nbst_debug(format, ...) \
         fprintf(stderr, "debug: %s: " format "\n", __func__, ## __VA_ARGS__); \
         fflush(stderr)
#  endif /* nbst_debug */

#  ifdef KEY_CMP
#    warning KEY_CMP
#  endif /* KEY_CMP */
#  ifdef KEY_FREE
#    warning KEY_FREE
#  endif /* KEY_FREE */
#  ifdef KEY_COPY
#    warning KEY_COMPY
#  endif /* KEY_COPY */
#  ifdef VALUE_T
#    ifdef VALUE_FREE
#      warning VALUE_FREE
#    endif /* VALUE_FREE */
#    ifdef VALUE_COPY
#      warning VALUE_COPY
#    endif /* VALUE_COPY */
#  endif /* VALUE_T */
#else
# define nbst_debug(format, ...)
#endif /* DEBUG */

/* static helper functions definition */
static nbst_ret_t
nbst_init(
    nbst_t * const tree,
    size_t         order);

static void
nbst_destroy(
    nbst_t * const tree);

static int
nbst_key_cmp(
    const KEY_T a,
    const KEY_T b);

static nbst_ret_t
nbst_key_copy(
          KEY_T *dest,
    const KEY_T  src);

#ifdef VALUE_T
static nbst_ret_t
nbst_value_copy(
          VALUE_T *dest,
    const VALUE_T  src);
#endif /* VALUE_T */

static nbst_node_t**
nbst_node_childs(
    nbst_node_t * const node,
    size_t              order);

#ifdef VALUE_T
static VALUE_T *
nbst_node_values(
    nbst_node_t * const node,
    size_t              order);
#endif /* VALUE_T */

static nbst_node_t*
nbst_mid_node_new(
    const size_t order);

static size_t
nbst_max_order();

static size_t
nbst_node_size(
    const size_t order);

static size_t
nbst_mid_node_size(
    const size_t order);

static size_t
nbst_end_node_size(
    const size_t order);

static void
nbst_mid_node_free(
          nbst_node_t * const node,
    const size_t              order,
          size_t              level);

static void
nbst_node_free(
          nbst_node_t * const node,
    const size_t              order,
    const size_t              level);

static nbst_node_t*
nbst_end_node_new(
    const size_t order);

static nbst_node_t*
nbst_mid_node_new(
    const size_t order);

static int
nbst_node_find_key(
          nbst_node_t * const node,
    const KEY_T               key,
          size_t *      const pos);


static void
nbst_end_node_remove(
    const size_t              order,
          nbst_node_t * const node,
    const size_t              pos);

static void
nbst_mid_node_remove(
    const size_t              order,
          size_t              level,
          nbst_node_t * const node,
    const size_t              pos);

static void
nbst_node_rebalance_left_to_right(
    const size_t              order,
    const size_t              level,
          KEY_T *       const midkey,
#ifdef VALUE_T
          VALUE_T *     const value,
#endif /* VALUE_T */
          nbst_node_t * const left,
          nbst_node_t * const right);

static void
nbst_node_rebalance_right_to_left(
    const size_t              order,
    const size_t              level,
          KEY_T *       const midkey,
#ifdef VALUE_T
          VALUE_T *     const value,
#endif /* VALUE_T */
          nbst_node_t * const left,
          nbst_node_t * const right);

static void
nbst_node_merge(
    const size_t              order,
    const size_t              level,
    const KEY_T               midkey,
#ifdef VALUE_T
    const VALUE_T             value,
#endif /* VALUE_T */
          nbst_node_t * const left,
          nbst_node_t * const right);

static nbst_foreach_ret_t
nbst_mid_node_foreach(
    const size_t                    order,
          size_t                    level,
          nbst_node_t       * const node,
          void              * const data,
          nbst_foreach_cb_f * const cb);

static nbst_foreach_ret_t
nbst_end_node_foreach(
    const size_t                    order,
          nbst_node_t       * const node,
          void              * const data,
          nbst_foreach_cb_f * const cb);

#ifdef KEY_FREE
static void
nbst_key_free(
    KEY_T key);
#endif /* KEY_FREE */

#ifdef VALUE_FREE
static void
nbst_value_free(
    VALUE_T value);
#endif /* VALUE_FREE */

#ifndef KEY_COPY
static nbst_ret_t
nbst_key_copy(
          KEY_T *dest,
    const KEY_T  src) {
  *dest = src;
  return NBST_OK;
}
#endif /* KEY_COPY */

#ifdef VALUE_T
# ifndef VALUE_COPY
static nbst_ret_t
nbst_value_copy(
          VALUE_T *dst,
    const VALUE_T  src) {
  *dst = src;
  return NBST_OK;
}
# endif /* VALUE_COPY */
#endif /* VALUE_T */

/* static helper functions implementation */
#ifndef KEY_CMP
static int
nbst_key_cmp(
    const KEY_T a,
    const KEY_T b) {
  int ret;

  if ( a < b )
    ret = -1;
  else if ( a == b )
    ret = 0;
  else
    ret = 1;

  return ret;
}
#endif /* KEY_CMP */

static nbst_node_t **
nbst_node_childs(
          nbst_node_t * const node,
    const size_t              order) {

#ifdef VALUE_T
  return
      (nbst_node_t**)
        &(nbst_node_values(node, order)[order]);
#else
  return (nbst_node_t**)&(node->keys[order]);
#endif /* VALUE_T */

}

#ifdef VALUE_T
static VALUE_T *
nbst_node_values(
          nbst_node_t * const node,
    const size_t              order) {

  return (VALUE_T*)&(node->keys[order]);

}
#endif /* VALUE_T */

static nbst_node_t *
nbst_mid_node_new(
    const size_t order) {

  nbst_node_t *ret = malloc(nbst_mid_node_size(order));

  nbst_debug("%p size: %zd", ret, nbst_mid_node_size(order));

  return ret;

}

static nbst_node_t *
nbst_end_node_new(
    const size_t order) {

  nbst_node_t *ret = malloc(nbst_end_node_size(order));

  nbst_debug("%p size: %zd", ret, nbst_end_node_size(order));

  return ret;

}

static size_t
nbst_max_order() {

  return
    (SIZE_MAX - sizeof(nbst_node_t)) / (
      sizeof(nbst_node_t*) + sizeof(KEY_T)
#ifdef VALUE_T
    + sizeof(VALUE_T)
#endif /* VALUE_T */
    );

}

static size_t
nbst_node_size(
    const size_t order) {

  return
    sizeof(nbst_node_t) + (order*sizeof(KEY_T))
#ifdef VALUE_T
    + (order * sizeof(VALUE_T))
#endif
    ;

}

static size_t
nbst_mid_node_size(
    const size_t order) {

  return nbst_node_size(order) +
         sizeof(nbst_node_t *) * (order + 1);

}

static size_t
nbst_end_node_size(
    const size_t order) {

  return nbst_node_size(order);

}

static void
nbst_mid_node_free(
          nbst_node_t * const node,
    const size_t              order,
          size_t              level) {

  nbst_debug(
      "%p %zu %zu",
      node, order, level);

  nbst_node_t **i = nbst_node_childs(node, order);
  nbst_node_t **e = &(i[node->fill + 1]);
  --level;
  for(; i<e; ++i)
    nbst_node_free(*i, order, level);

}

static void
nbst_node_free(
          nbst_node_t * const node,
    const size_t              order,
    const size_t              level) {

  nbst_debug(
      "%p %zu %zu",
      node, order, level);

#ifdef KEY_FREE
  {
    KEY_T *i, *e;
    for(i=node->keys, e=&(i[node->fill]); i < e; ++i) {
      nbst_debug("free key %zd", i - node->keys);
      nbst_key_free(*i);
    }
  }
#endif /* KEY_FREE */

#ifdef VALUE_FREE
  {
    VALUE_T *vals, *i, *e;
    vals = nbst_node_values(node, order);
    for(i = vals, e = &(vals[node->fill]);
        i < e;
        ++i) {

      nbst_debug("free value %zd", i - vals);
      nbst_value_free(*i);

    }
  }
#endif /* VALUE_FREE */

  if ( level )
    nbst_mid_node_free(node, order, level);

  free(node);

}

static int
nbst_node_find_key(
          nbst_node_t * const node,
    const KEY_T               key,
          size_t *      const pos) {

  int ret = -1;
  size_t p = 0;
  size_t min = 0;
  size_t max = node->fill;
  KEY_T *keys = node->keys;

  nbst_debug(
      "node@%p:{fill:%zu}",
      node, node->fill);

  while ( nbst_likely(min < max) ) {

    p = (max + min) / 2;
    ret = nbst_key_cmp(key, keys[p]);

    if ( nbst_unlikely(ret == 0) ) break;

    if ( ret < 0 )
      max = p;
    else
      min = ++p;

  }

  nbst_debug(
      "cmp: %d (%p) = %zu",
      ret, node, p);

  *pos = p;
  return ret;

}

static nbst_ret_t
nbst_end_node_split(
          nbst_node_t *  const node,
    const size_t               order,
          KEY_T *        const split_key,
#ifdef VALUE_T
          VALUE_T *      const split_val,
#endif /* VALUE_T */
          nbst_node_t ** const right) {

  __label__ end;

  nbst_ret_t ret = NBST_OK;
  nbst_node_t *new_right;
  size_t left_fill;
  size_t right_fill;

  nbst_debug(
      "node@%p:{fill:%zu}",
      node, node->fill);

  new_right = nbst_end_node_new(order);

  if ( nbst_unlikely(!new_right) ) {

    ret = NBST_NOMEM;
    goto end;

  }

  left_fill  = order / 2;
  right_fill = order - left_fill - 1;

  memcpy(
      new_right->keys,
      &(node->keys[left_fill + 1]),
      right_fill * sizeof(new_right->keys[0]));

#ifdef VALUE_T
  VALUE_T *vals = nbst_node_values(node, order);

  memcpy(
      nbst_node_values(new_right, order),
      &(vals[left_fill + 1]),
      right_fill * sizeof(vals[0]));
#endif /* VALUE_T */

  node->fill      = left_fill;
  new_right->fill = right_fill;
  *split_key      = node->keys[left_fill];
#ifdef VALUE_T
  *split_val      = vals[left_fill];
#endif /* VALUE_T */
  *right          = new_right;

end:
  return ret;

}

static void
nbst_mid_node_inject(
          nbst_node_t * const node,
    const KEY_T               key,
#ifdef VALUE_T
    const VALUE_T             val,
#endif /* VALUE_T */
    const size_t              order,
    const size_t              pos,
          nbst_node_t * const right) {

  nbst_debug(
      "node@%p:{fill:%zu}, "
      "order: %zu, "
      "pos: %zu, "
      "right@%p:{fill:%zu}",
      node, node->fill,
      order,
      pos,
      right, right->fill);

  nbst_node_t **childs = nbst_node_childs(node, order);
#ifdef VALUE_T
  VALUE_T *vals = nbst_node_values(node, order);
#endif /* VALUE_T */


  if ( nbst_likely(pos < node->fill) ) {

    size_t shift = node->fill - pos;

    memmove(
        &(node->keys[pos+1]),
        &(node->keys[pos]),
        shift * sizeof(node->keys[0]));

#ifdef VALUE_T
    memmove(
        &(vals[pos+1]),
        &(vals[pos]),
        shift * sizeof(vals[0]));
#endif /* VALUE_T */

    memmove(
        &(childs[pos+2]),
        &(childs[pos+1]),
        shift * sizeof(childs[0]));

  }

  node->keys[pos] = key;
#ifdef VALUE_T
  vals[pos]       = val;
#endif /* VALUE_T */
  childs[pos+1]   = right;

  ++(node->fill);

}

static void
nbst_end_node_inject(
          nbst_node_t * const node,
    const KEY_T               key,
#ifdef VALUE_T
    const VALUE_T             value,
#endif /* VALUE_T */
    const size_t              order,
    const size_t              pos) {

  nbst_debug(
      "node@%p: {fill: %zu},"
      "order: %zu, "
      "pos: %zu",
      node, node->fill,
      order,
      pos);

#ifdef VALUE_T
  VALUE_T *vals = nbst_node_values(node, order);
#endif /* VALUE_T */

  if ( nbst_likely(pos < node->fill) ) {

    size_t shift = node->fill - pos;
    memmove(
        &(node->keys[pos+1]),
        &(node->keys[pos]),
        shift * sizeof(node->keys[0]));

#ifdef VALUE_T
    memmove(
        &(vals[pos+1]),
        &(vals[pos]),
        shift * sizeof(vals[0]));
#endif /* VALUE_T */

  }

#ifdef VALUE_T
  vals[pos]       = value;
#endif /* VALUE_T */
  node->keys[pos] = key;
  ++(node->fill);

}

static nbst_ret_t
nbst_mid_node_split(
          nbst_node_t *  const node,
    const size_t               order,
          KEY_T *        const split_key,
#ifdef VALUE_T
          VALUE_T *      const split_val,
#endif /* VALUE_T */
          nbst_node_t ** const right) {

  __label__ end;

  nbst_ret_t ret = NBST_OK;
  nbst_node_t *new_right;
  size_t left_fill;
  size_t right_fill;
  nbst_node_t **left_childs;
  nbst_node_t **right_childs;

  nbst_debug(
      "node@%p: {fill: %zu}, "
      "order: %zu",
      node, node->fill,
      order);

  new_right = nbst_mid_node_new(order);

  if ( nbst_unlikely(!new_right) ) {

    ret = NBST_NOMEM;
    goto end;

  }

  left_fill = order / 2;
  right_fill = order - left_fill - 1;
  left_childs = nbst_node_childs(node, order);
  right_childs = nbst_node_childs(new_right, order);

  memcpy(
      new_right->keys,
      &(node->keys[left_fill + 1]),
      right_fill * sizeof(new_right->keys[0]));

#ifdef VALUE_T
  VALUE_T *vals = nbst_node_values(node, order);

  memcpy(
      nbst_node_values(new_right, order),
      &(vals[left_fill + 1]),
      right_fill * sizeof(VALUE_T));
#endif /* VALUE_T */

  memcpy(
      right_childs,
      &(left_childs[left_fill + 1]),
      (right_fill + 1) * sizeof(right_childs[0]));

  node->fill      = left_fill;
  new_right->fill = right_fill;
  *split_key      = node->keys[left_fill];
#ifdef VALUE_T
  *split_val      = vals[left_fill];
#endif /* VALUE_T */
  *right          = new_right;

end:
  return ret;

}

static nbst_ret_t
nbst_root_node_split(
          nbst_node_t *  const node,
    const size_t               order,
    const size_t               level,
          nbst_node_t ** const retnode) {

  __label__ end;

  nbst_ret_t ret = NBST_OK;
  nbst_node_t *next, *rnode, **childs;
  KEY_T   skey;
#ifdef VALUE_T
  VALUE_T sval;
#endif /* VALUE_T */

  nbst_debug(
      "node@%p: {fill: %zu}, "
      "order: %zu, "
      "level: %zu",
      node, node->fill,
      order,
      level);

  next = nbst_mid_node_new(order);

  if ( nbst_unlikely(!next) ) {

    ret = NBST_NOMEM;
    goto end;

  }

  nbst_midend(
      level,
      node_split,
      node,
      order,
      &skey,
#ifdef VALUE_T
      &sval,
#endif /* VALUE_T */
      &rnode);

  if ( nbst_unlikely(ret != NBST_OK) ) {

    free(next);
    goto end;

  }

  next->keys[0]                    = skey;
#ifdef VALUE_T
  nbst_node_values(next, order)[0] = sval;
#endif /* VALUE_T */
  next->fill                       = 1;

  childs    = nbst_node_childs(next, order);
  childs[0] = node;
  childs[1] = rnode;

  *retnode = next;

end:
  return ret;

}

static void
nbst_end_node_remove(
    const size_t              order,
          nbst_node_t * const node,
    const size_t              pos) {

  nbst_debug(
      "order: %zu, "
      "node@%p: {fill: %zu}, "
      "pos: %zu",
      order, node, node->fill, pos);

#ifdef KEY_FREE
  nbst_debug("free key %zd", pos);
  nbst_key_free(node->keys[pos]);
#endif /* KEY_FREE */

#ifdef VALUE_T
  VALUE_T *vals = nbst_node_values(node, order);
#endif /* VALUE_T */

#ifdef VALUE_FREE
  nbst_debug("free value %zd", pos);
  nbst_value_free(vals[pos]);
#endif /* VALUE_FREE */

  --(node->fill);

  if ( nbst_likely( pos < node->fill ) ) {

    size_t shift = node->fill - pos;

    /* shift keys */
    memmove(
        &(node->keys[pos]),
        &(node->keys[pos + 1]),
        shift * sizeof(node->keys[0]));

#ifdef VALUE_T
    /* shift values */
    memmove(
        &(vals[pos]),
        &(vals[pos + 1]),
        shift * sizeof(vals[0]));
#endif /* VALUE_T */

  }

}


static void
nbst_mid_node_remove(
    const size_t       order,
          size_t       level,
          nbst_node_t *node,
    const size_t       pos) {

  nbst_node_t *cur;
  nbst_node_t *left;
  nbst_node_t  *right;
  nbst_node_t **childs;
  const size_t n = order / 2;

  nbst_debug(
      "order: %zu, "
      "node@%p: {fill: %zu}, "
      "pos: %zu",
      order, node, node->fill, pos);

#ifdef KEY_FREE
  nbst_debug("free key: %zd", pos);
  nbst_key_free(node->keys[pos]);
#endif /* KEY_FREE */

#ifdef VALUE_FREE
  nbst_debug("free value: %zd", pos);
  nbst_value_free(nbst_node_values(node, order)[pos]);
#endif /* VALUE_FREE */

  childs =
      nbst_node_childs(
          node,
          order);

  /* move biggest key from left subtree to the place, where we remove key */

  nbst_debug("get biggest key from left subtree");

  /* traverse tree */

  for(right = childs[pos], --level; /* begin with left subtree */
      nbst_likely(level);
      --level) {

    cur = right;

    nbst_debug(
        "cur node: %p",
        cur);

    childs =
        nbst_node_childs(
            cur,
            order);

    right = childs[cur->fill]; /* next node is most right child node */

    if ( nbst_unlikely(right->fill <= n) ) {

      /* next node might underflow */

      nbst_debug("next node might underflow");

      left = childs[cur->fill - 1];

      if ( left->fill <= n) {

        /* merge */
        nbst_node_merge(
            order,
            level - 1,
            cur->keys[cur->fill - 1],
#ifdef VALUE_T
            nbst_node_values(cur, order)[cur->fill - 1],
#endif /* VALUE_T */
            left,
            right);

        --(cur->fill);
        right = left;

      } else {

        nbst_debug("rebalance");

        /* rebalance */
        nbst_node_rebalance_left_to_right(
            order,
            level - 1,
            &(cur->keys[cur->fill - 1]),
#ifdef VALUE_T
            &(nbst_node_values(cur, order)[cur->fill - 1]),
#endif /* VALUE_T */
            left,
            right);

      }

    }

  }

  /* end node reached */
  --(right->fill);

  node->keys[pos]                    = right->keys[right->fill];
#ifdef VALUE_T
  nbst_node_values(node, order)[pos] = nbst_node_values(right, order)[right->fill];
#endif /* VALUE_T */


}

static void
nbst_node_merge(
    const size_t              order,
    const size_t              level,
    const KEY_T               midkey,
#ifdef VALUE_T
    const VALUE_T             midval,
#endif /* VALUE_T */
          nbst_node_t * const left,
          nbst_node_t * const right) {

  nbst_debug(
      "order: %zu, "
      "level: %zu, "
      "left@%p: {fill: %zu}, "
      "right@%p: {fill: %zu}",
      order, level,
      left, left->fill,
      right, right->fill);

#ifdef VALUE_T
  VALUE_T *left_vals = nbst_node_values(left, order);
#endif /* VALUE_T */

  left->keys[left->fill] = midkey;
#ifdef VALUE_T
  left_vals[left->fill]  = midval;
#endif /* VALUE_T */
  ++(left->fill);

  memcpy(
      &(left->keys[left->fill]),
      right->keys,
      right->fill * sizeof(left->keys[0]));

#ifdef VALUE_T
  memcpy(
      &(left_vals[left->fill]),
      nbst_node_values(right, order),
      right->fill * sizeof(left_vals[0]));
#endif /* VALUE_T */

  if ( nbst_likely(level) ) {

    nbst_node_t **left_childs  = nbst_node_childs(left, order);
    nbst_node_t **right_childs = nbst_node_childs(right, order);

    memcpy(
        &(left_childs[left->fill]),
        right_childs,
        (right->fill + 1) * sizeof(left_childs[0]));

   }

   left->fill += right->fill;

  free(right);

  nbst_debug(
      "new merged node: %p: {fill: %zu}",
      left, left->fill);

}

static void
nbst_node_rebalance_left_to_right(
    const size_t              order,
    const size_t              level,
          KEY_T *       const midkey,
#ifdef VALUE_T
          VALUE_T *     const midval,
#endif /* VALUE_T */
          nbst_node_t * const left,
          nbst_node_t * const right) {

  nbst_debug(
      "rebalance from %p to %p @ %zd",
      left, right, level);

#ifdef VALUE_T
  VALUE_T *right_vals = nbst_node_values(right, order);
  VALUE_T *left_vals  = nbst_node_values(left, order);
#endif /* VALUE_T */

  size_t n = (left->fill - right->fill) / 2;
  if ( !n ) { n = 1; }

  /* make place for new elements in right node */
  memmove(
      &(right->keys[n]), /* to elem n+1 */
      right->keys,       /* from start */
      right->fill * sizeof(right->keys[0])); /* as many as we have now */

#ifdef VALUE_T
  memmove(
      &(right_vals[n]),
      right_vals,
      right->fill * sizeof(right_vals[0]));
#endif /* VALUE_T */

  if ( n > 1 ) {
    /* copy new elements to the right node */
    memcpy(
        right->keys, /* at the beginning */
        &(left->keys[left->fill + 1 - n]), /* from the end - n of the left node */
        (n - 1) * sizeof(right->keys[0])); /* n - 1 elements (1 go up and 1 come from above) */

#ifdef VALUE_T
    memcpy(
        right_vals,
        &(left_vals[left->fill + 1 - n]),
        (n - 1) * sizeof(right_vals[0]));
#endif /* VALUE_T */

  }

  right->keys[n - 1] = *midkey; /* one from above */
  *midkey            = left->keys[left->fill - n]; /* one go up */

#ifdef VALUE_T
  right_vals[n - 1] = *midval;
  *midval           = left_vals[left->fill - n];
#endif /* VALUE_T */

  if ( nbst_likely(level) ) {

    nbst_node_t **left_childs =
        nbst_node_childs(left, order);

    nbst_node_t **right_childs =
        nbst_node_childs(right, order);

    /* meke place for new childs */
    memmove(
        &(right_childs[n]),
        right_childs,
        (right->fill + 1) * sizeof(right_childs[0]));

    /* copy new childs to the right node */
    memcpy(
        right_childs,
        &(left_childs[left->fill - n + 1]),
        n * sizeof(right_childs[0]));

  }

  left->fill  -= n;
  right->fill += n;

}

static void
nbst_node_rebalance_right_to_left(
    const size_t              order,
    const size_t              level,
          KEY_T *       const midkey,
#ifdef VALUE_T
          VALUE_T *     const midval,
#endif /* VALUE_T */
          nbst_node_t * const left,
          nbst_node_t * const right) {

  nbst_debug(
      "rebalance from %p to %p @ %zd",
      right, left, level);

  size_t n = (right->fill - left->fill) / 2;
  if ( !n ) { n = 1; }

  left->keys[left->fill] = *midkey;
  *midkey                = right->keys[n - 1];

#ifdef VALUE_T
  VALUE_T *right_vals = nbst_node_values(right, order);
  VALUE_T *left_vals  = nbst_node_values(left, order);

  left_vals[left->fill] = *midval;
  *midval               = right_vals[n - 1];
#endif /* VALUE_T */

  if ( n > 1 ) {
    memcpy(
        &(left->keys[left->fill + 1]),
        right->keys,
        (n - 1) * sizeof(left->keys[0]));

#ifdef VALUE_T
    memcpy(
        &(left_vals[left->fill + 1]),
        right_vals,
        (n - 1) * sizeof(left_vals[0]));
#endif /* VALUE_T */
  }

  memmove(
      right->keys,
      &(right->keys[n]),
      (right->fill - n) * sizeof(right->keys[0]));

#ifdef VALUE_T
  memmove(
      right_vals,
      &(right_vals[n]),
      (right->fill - n) * sizeof(right_vals[0]));
#endif /* VALUE_T */

  if ( nbst_likely(level) ) {

    nbst_node_t **left_childs =
        nbst_node_childs(left, order);

    nbst_node_t **right_childs =
        nbst_node_childs(right, order);

    memcpy(
        &(left_childs[left->fill + 1]),
        right_childs,
        n * sizeof(left_childs[0]));

    memmove(
        right_childs,
        &(right_childs[n]),
        (right->fill + 1 - n) * sizeof(right_childs[0]));

  }

  right->fill -= n;
  left->fill  += n;

}


/* ==administrative function== */
nbst_func nbst_ret_t
nbst_init(
    nbst_t * const tree,
    size_t         order) {

  __label__ end;

  nbst_node_t *root;
  nbst_ret_t ret = NBST_OK;

  nbst_debug(
      "tree@%p, "
      "order: %zu",
      tree, order);

  if ( nbst_unlikely((order > nbst_max_order())) ) {

    ret = NBST_OVERFLOW;
    goto end;

  }

  if ( order == 0 )
    order = 61; /* should be good default value */
  else if ( order < 3 )
    order = 3;
  else if ( order % 2 != 1 )
  /* order = 2n + 1 (so must be even) */
    --order;

  root = nbst_end_node_new(order);

  if ( nbst_unlikely(!root) ) {

    ret = NBST_NOMEM;
    goto end;

  }

  root->fill = 0;

  tree->order = order;
  tree->length = 0;
  tree->level = 0;
  tree->root = root;

end:
  return ret;
}

nbst_func nbst_ret_t
nbst_new(
          nbst_t ** const ret_tree,
    const size_t          order) {

  __label__ end, err;

  nbst_ret_t ret;

  nbst_debug(
      "order: %zu",
      order);

  nbst_t *tree = malloc(sizeof(*tree));

  if ( nbst_unlikely(tree == NULL) ) {

    ret = NBST_NOMEM;
    goto err;

  }

  ret = nbst_init(tree, order);

  if ( nbst_unlikely(ret != NBST_OK) )
    goto err;

  *ret_tree = tree;

end:
  return ret;

err:
  if ( tree )
    free(tree);
  goto end;

}

nbst_func void
nbst_destroy(
    nbst_t * const tree) {

  nbst_debug(
      "tree@%p: {root: %p, len: %zu, level: %zu}",
      tree, tree->root, tree->length, tree->level);

  nbst_node_free(tree->root, tree->order, tree->level);
  tree->root = NULL;

}

nbst_func void nbst_free(nbst_t * const tree) {

  nbst_debug(
      "tree@%p",
      tree);

  nbst_destroy(tree);
  free(tree);

}

/* ==data manipulation== */
static inline nbst_ret_t
nbst_insertset(
          nbst_t * const tree,
    const KEY_T          key
#ifdef VALUE_T
   ,const VALUE_T        value,
    const int            set
#endif /* VALUE_T */
  ) {

  __label__ end, exists;

  nbst_ret_t   ret;
  nbst_node_t *node, *next, *rnode;
  size_t       level;
  size_t       order;
  size_t       pos;
  int          cmp;
  KEY_T        skey;
#ifdef VALUE_T
  VALUE_T      sval;
#endif /* VALUE_T */

  nbst_debug(
      "tree@%p: {root: %p, len: %zu, level: %zu}",
      tree, tree->root, tree->length, tree->level);

  ret = NBST_OK;

  node  = tree->root;
  level = tree->level;
  order = tree->order;

  /* tree is empty, so just insert new key */
  if ( nbst_unlikely(!(tree->length)) ) {

    ret = nbst_key_copy(
              &(node->keys[0]),
              key);

    if ( nbst_unlikely(ret != NBST_OK) ) {
      nbst_debug("key value copy failed");
      goto end;
    }

#ifdef VALUE_T
    ret = nbst_value_copy(
              &(nbst_node_values(node, order)[0]),
              value);

    if ( nbst_unlikely(ret != NBST_OK) ) {
      nbst_debug("value copy failed");
#ifdef KEY_FREE
      nbst_key_free(node->keys[0]);
#endif /* KEY_FREE */
      goto end;
    }
#endif /* VALUE_T */

    ++(node->fill);
    ++(tree->length);
    goto end;

  }

  /* check, if root node need to be split */
  if ( nbst_unlikely(node->fill == order) ) {

    ret = nbst_root_node_split(
              node,
              order,
              level,
              &node);
    tree->root = node;
    ++(tree->level);
    ++level;

  }

  while(1) {

    cmp = nbst_node_find_key(node, key, &pos);

    if ( nbst_unlikely(cmp == 0) )
      goto exists;

    if ( nbst_likely(level) ) {

      /* peek child of mid node */
      next = nbst_node_childs(node,order)[pos];

      /* need to split node, if fill >= 2n */
      if ( nbst_unlikely(next->fill >= order) ) {

        nbst_midend(
            (level - 1),
            node_split,
            next,
            order,
            &skey,
#ifdef VALUE_T
            &sval,
#endif /* VALUE_T */
            &rnode);

        if ( nbst_unlikely(ret != NBST_OK) )
          goto end;

        nbst_mid_node_inject(
            node,
            skey,
#ifdef VALUE_T
            sval,
#endif /* VALUE_T */
            order,
            pos,
            rnode);

        cmp = nbst_key_cmp(key, skey);

        if ( nbst_unlikely(cmp == 0) )
          goto exists;
        else if ( cmp > 0 )
          node = rnode;
        else
          node = next;

      } else
        node = next;

      --level;

    } else {

      /* end node, insert key */
      ret = nbst_key_copy(&skey, key);
      if ( ret != NBST_OK ) goto end;

#ifdef VALUE_T
      ret = nbst_value_copy(&sval, value);
      if ( ret != NBST_OK ) {
        nbst_debug("value copy failed");
#ifdef KEY_FREE
        nbst_key_free(key);
#endif /* KEY_FREE */
        goto end;
      }
#endif /* VALUE_T */

      nbst_end_node_inject(
          node,
          skey,
#ifdef VALUE_T
          sval,
#endif /* VALUE_T */
          order,
          pos);
      ++(tree->length);
      break;

    }
  }

end:
  return ret;

exists:
#ifdef VALUE_T
  if ( set ) {
    VALUE_T *val = &(nbst_node_values(node, order)[pos]);
#ifdef VALUE_FREE
    nbst_value_free(*val);
#endif /* VALUE_FREE */
    ret = nbst_value_copy(val, value);
  } else {
#endif /* VALUE_T */
    ret = NBST_EXISTS;
#ifdef VALUE_T
  }
#endif /* VALUE_T */
  goto end;

}

#ifdef VALUE_T
nbst_func nbst_ret_t
nbst_insert(
          nbst_t * const tree,
    const KEY_T          key,
    const VALUE_T        value) {
  return nbst_insertset(tree, key, value, 0);
}

nbst_func nbst_ret_t
nbst_set( nbst_t * const tree,
    const KEY_T          key,
    const VALUE_T        value) {
  return nbst_insertset(tree, key, value, 1);
}
#else
nbst_func nbst_ret_t
nbst_insert(
          nbst_t * const tree,
    const KEY_T          key) {
  return nbst_insertset(tree, key);
}
#endif /* VALUE_T */

nbst_func nbst_ret_t
nbst_remove(
          nbst_t * const tree,
    const KEY_T          key) {

  __label__ end;

  nbst_ret_t    ret;
  nbst_node_t  *node  = tree->root;
  nbst_node_t  *next, *other;
  nbst_node_t **childs;
  size_t        level = tree->level;
  size_t        order = tree->order;
  size_t        pos;
  int           cmp;
  const size_t  n = order / 2;

  nbst_debug(
      "tree@%p: {root: %p, len: %zu, level: %zu}",
      tree, tree->root, tree->length, tree->level);

  ret = NBST_NOTFOUND;

  /* check if the tree is empty */
  if ( nbst_unlikely(!tree->length) ) {
    goto end;
  }

  node = tree->root;

  while(1) {

    nbst_debug(
        "current node: %p@%zd",
        node, level);

    cmp = nbst_node_find_key(node, key, &pos);

    nbst_debug("found @ %zd", pos);

    if ( nbst_unlikely(!level) ) {

      /* END NODE */

      if ( nbst_likely(!cmp) ) {

        nbst_debug(
            "key found in end node");

        nbst_end_node_remove(
            order,
            node,
            pos);

        --(tree->length);

        ret = NBST_OK;

      }

      break;

    } else {

      /* MID NODE */

      nbst_debug(
          "remove from mid node");

      childs =
          nbst_node_childs(
              node,
              order);

      next = childs[pos];

      nbst_debug("next@%p {fill: %zd}", next, next->fill);

      if ( nbst_unlikely(next->fill <= n ) ) {

        /* NEXT NODE UNDERFLOW */

        nbst_debug(
            "next node %p@%zd might underflow",
            next, pos);

        if ( nbst_unlikely(pos == node->fill) ) {

          /* MOST RIGHT NODE */

          nbst_debug(
              "handle most right child");

          --pos;

          other = childs[pos];

          nbst_debug("other@%p {fill: %zd}", other, other->fill);

          if ( other->fill <= n ) {

            /* merge */

            nbst_debug(
                "merge nodes %p and %p",
                other, next);

            nbst_node_merge(
                order,
                level - 1,
                node->keys[pos],
#ifdef VALUE_T
                nbst_node_values(node, order)[pos],
#endif /* VALUE_T */
                other,
                next);

            next = other;

            --(node->fill);

            if ( nbst_unlikely(!(node->fill)) ) {

              nbst_debug(
                  "root node is empty, decrease level");

              free(tree->root);
              tree->root = next;
              --(tree->level);

            }

          } else {

            /* rebalance */

            nbst_debug(
                "rebalance from %p to %p",
                other, next);

            nbst_node_rebalance_left_to_right(
                order,
                level - 1,
                &(node->keys[pos]),
#ifdef VALUE_T
                &(nbst_node_values(node, order)[pos]),
#endif /* VALUE_T */
                other,
                next);

          }

          /* END MOST RIGHT NODE */

        } else {

          /* NOT MOST RIGHT NODE */

          nbst_debug(
              "handle not most right child");

          other = childs[pos + 1];

          /* when we rebalance or merge, found key would
             go to next node */
          cmp = 1;

          if ( other->fill <= n ) {

            /* merge */

            nbst_debug(
                "merge nodes %p and %p",
                next, other);

            nbst_node_merge(
                order,
                level - 1,
                node->keys[pos],
#ifdef VALUE_T
                nbst_node_values(node, order)[pos],
#endif /* VALUE_T */
                next,
                other);

            --(node->fill);

            if ( nbst_likely(node->fill) ) {

              size_t shift = node->fill - pos;

              memmove(
                  &(node->keys[pos]),
                  &(node->keys[pos+1]),
                  shift * sizeof(node->keys[0]));

#ifdef VALUE_T
              VALUE_T *vals = nbst_node_values(node, order);

              memmove(
                  &(vals[pos]),
                  &(vals[pos+1]),
                  shift * sizeof(vals[0]));
#endif /* VALUE_T */

              memmove(
                  &(childs[pos+1]),
                  &(childs[pos+2]),
                  (node->fill - pos) * sizeof(childs[0]));

            } else {

              nbst_debug(
                "root node is empty, decrease level");

              free(tree->root);
              tree->root = next;
              --(tree->level);

            }

          } else {

            /* rebalance */

            nbst_debug(
                "rebalance from %p to %p",
                other, next);

            nbst_node_rebalance_right_to_left(
                order,
                level - 1,
                &(node->keys[pos]),
#ifdef VALUE_T
                &(nbst_node_values(node, order)[pos]),
#endif /* VALUE_T */
                next,
                other);

          }


          /* END NOT MOST RIGHT NODE */

        }

        /* END NEXT NODE UNDERFLOW */

      }

      if ( nbst_unlikely(!cmp) ) {

        /* key found */

        nbst_debug(
            "key found in mid node, remove it");

        nbst_mid_node_remove(
            order,
            level,
            node,
            pos);

        --(tree->length);

        ret = NBST_OK;

        break;

      }

      /* END MID NODE */
    }

    /* move 1 level down */
    --level;
    node = next;

  }

end:
  return ret;

}

/* ==data retrive== */
nbst_func nbst_ret_t
nbst_search(
    const nbst_t * const tree,
    const KEY_T          key,
          KEY_T *  const ret_key
#ifdef VALUE_T
   ,      VALUE_T * const value
#endif /* VALUE_T */
          ) {

  nbst_ret_t ret;
  nbst_node_t *node = tree->root;
  size_t level = tree->level;
  size_t order = tree->order;
  size_t pos;
  int cmp;

  nbst_debug(
      "tree@%p: {root: %p, len: %zu, level: %zu}",
      tree, tree->root, tree->length, tree->level);

  ret = NBST_NOTFOUND;

  while(1) {

    cmp = nbst_node_find_key(node, key, &pos);

    if ( nbst_unlikely(cmp == 0) ) {

      if ( ret_key )
        *ret_key = node->keys[pos];
#ifdef VALUE_T
      if ( value )
        *value   = nbst_node_values(node, order)[pos];
#endif /* VALUE_T */
      ret = NBST_OK;
      break;

    }

    if ( nbst_unlikely(level == 0) )
      break;

    node = nbst_node_childs(node, order)[pos];
    --level;

  }

  return ret;

}

static nbst_foreach_ret_t
nbst_mid_node_foreach(
    const size_t                    order,
          size_t                    level,
          nbst_node_t       * const node,
          void              * const data,
          nbst_foreach_cb_f * const cb) {

  __label__ end;

  nbst_foreach_ret_t ret;
  size_t i, e;
  nbst_node_t **childs;

  --level;

  i = 0;
  e = node->fill;

  childs =
      nbst_node_childs(node, order);

  if ( nbst_likely(level) ) {

    for(; i<e; ++i) {

      ret =
          nbst_mid_node_foreach(
              order,
              level,
              childs[i],
              data,
              cb);

      if ( nbst_unlikely(ret) ) goto end;

#ifdef VALUE_T
      ret = cb(node->keys[i], nbst_node_values(node, order)[i], data);
#else
      ret = cb(node->keys[i], data);
#endif /* VALUE_T */

      if ( nbst_unlikely(ret) ) goto end;

    }

    ret =
        nbst_mid_node_foreach(
            order,
            level,
            childs[i],
            data,
            cb);

  } else {

    for(; i<e; ++i) {

      ret =
          nbst_end_node_foreach(
              order,
              childs[i],
              data,
              cb);

      if ( nbst_unlikely(ret) ) goto end;

#ifdef VALUE_T
      ret = cb(node->keys[i], nbst_node_values(node, order)[i], data);
#else
      ret = cb(node->keys[i], data);
#endif /* VALUE_T */

      if ( nbst_unlikely(ret) ) goto end;

    }

    ret =
        nbst_end_node_foreach(
            order,
            childs[i],
            data,
            cb);

  }

end:
  return ret;

}

static nbst_foreach_ret_t
nbst_end_node_foreach(
    const size_t                    order,
          nbst_node_t       * const node,
          void              * const data,
          nbst_foreach_cb_f * const cb) {

  nbst_foreach_ret_t ret = NBST_FOREACH_OK;
  KEY_T *i, *e;

#ifdef VALUE_T
  VALUE_T *v = nbst_node_values(node, order);
#endif /* VALUE_T */

  for(i = node->keys, e = &(node->keys[node->fill]);
      i < e;
      ++i
#ifdef VALUE_T
     ,++v
#endif /* VALUE_T */
    ) {

#ifdef VALUE_T
    ret = cb(*i, *v, data);
#else
    ret = cb(*i, data);
#endif /* VALUE_T */

    if ( nbst_unlikely(ret) ) break;

  }

  return ret;

}

nbst_func nbst_ret_t
nbst_foreach(
    const nbst_t            * const tree,
          void              * const data,
          nbst_foreach_cb_f * const cb) {

  nbst_foreach_ret_t ret;
  nbst_ret_t rret;

  nbst_debug(
      "tree@%p: {root: %p, len: %zu, level: %zu}",
      tree, tree->root, tree->length, tree->level);

  if ( nbst_unlikely(!tree->length) ) {
    rret = NBST_OK;
    goto end;
  }

  if ( nbst_likely(tree->level) ) {

    ret =
        nbst_mid_node_foreach(
            tree->order,
            tree->level,
            tree->root,
            data,
            cb);

  } else {

    ret =
        nbst_end_node_foreach(
            tree->order,
            tree->root,
            data,
            cb);

  }

  rret =
      (ret > NBST_FOREACH_STOP_OK) ?
        NBST_FOREACH_CB_ERROR :
        NBST_OK;

end:
  return rret;

}

/* ==meta functions== */
nbst_func size_t
nbst_length(
    const nbst_t * const tree) {

  return tree->length;

}

#ifndef NBST_DONT_UNDEF
# include <nbst/nbst_undef.h>
#endif /* NBST_DONT_UNDEF */
