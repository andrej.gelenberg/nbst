/* Copyright (c) 2013-2014 Andrej Gelenberg <andrej.gelenberg@udo.edu> */

#ifndef _TESTS_MSGS_H
#define _TESTS_MSGS_H

#define csi(n)  "\e[" #n "m"

#define red    csi(31)
#define green  csi(32)
#define yellow csi(33)
#define blue   csi(34)
#define reset  csi(0)

#define error(msg, ...) \
    printf( \
        red msg reset "\n", ## __VA_ARGS__)

#define pass(msg, ...) \
    printf( \
        green msg reset "\n", ## __VA_ARGS__)

#define info(msg, ...) \
    printf( \
        blue msg reset "\n", ## __VA_ARGS__)

#define warn(msg, ...) \
     printf( \
         yellow msg reset "\n", ## __VA_ARGS__)

#define fail(msg, ...) \
    { error(msg, ## __VA_ARGS__); goto err; }

#define dbg(msg, ...) \
    printf(msg "\n", ## __VA_ARGS__)


#endif /* _TESTS_MSGS_H */
