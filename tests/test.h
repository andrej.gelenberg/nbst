/* Copyright (c) 2013-2014 Andrej Gelenberg <andrej.gelenberg@udo.edu> */

#ifdef VALUE_T
# define __nbst_name(key_type, data_type, suffix) nbst_ ## key_type ## _ ## data_type ## _ ##suffix
# define _nbst_name(key_type, data_type, suffix)  __nbst_name(key_type, data_type, suffix)
# define nbst_name(suffix) _nbst_name(KEY_T, VALUE_T, suffix)
#else /* VALUE_T */
# define __nbst_name(key_type, suffix) nbst_ ## key_type ## _ ## suffix
# define _nbst_name(key_type, suffix) __nbst_name(key_type, suffix)
# define nbst_name(suffix) _nbst_name(KEY_T, suffix)
#endif /* VALUE_T */

#define nbst_t            nbst_name(t)
#define nbst_insert       nbst_name(insert)
#define nbst_search       nbst_name(search)
#define nbst_insert_test  nbst_name(insert_test)
#define nbst_check_tree   nbst_name(check_tree)
#define nbst_key_cmp      nbst_name(key_cmp)
#define nbst_remove       nbst_name(remove)
#define nbst_remove_test  nbst_name(remove_test)
#define nbst_value_cmp    nbst_name(value_cmp)

#ifdef DEBUG
#  define nbst_key_print    nbst_name(key_print)
#  define nbst_value_print  nbst_name(value_print)

#  ifndef nbst_debug
#    include <stdio.h>
#    define nbst_debug(format, ...) \
         fprintf(stderr, "debug: %s: " format "\n", __func__, ## __VA_ARGS__); \
         fflush(stderr)
#  endif /* nbst_debug */

static int
nbst_key_print(
    const KEY_T key);

#ifdef VALUE_T
static int
nbst_value_cmp(
    const VALUE_T a,
    const VALUE_T b);

static int
nbst_value_print(
    const VALUE_T value);
#endif /* VALUE_T */

#else
# define nbst_debug(format, ...)
# define nbst_key_print(...)
# define nbst_value_print(...)
#endif /* DEBUG */

#define min(a, b) (((a) < (b)) ? (a) : (b))

static int
nbst_insert_test(
    nbst_t *tree,
    KEY_T  *buf) {

  __label__ end, err;
  int ret = 0;
  size_t i;
  nbst_ret_t c;

  for(i = 0; i < size; ++i) {

    nbst_debug("insert key %zd", i);
    nbst_key_print(buf[i]);

    c = nbst_insert(
            tree,
            buf[i]
#ifdef VALUE_T
           ,buf[i]
#endif /* VALUE_T */
            );

    if ( nbst_unlikely(c != NBST_OK) )
      fail("fail to insert at %zd: %s", i, nbst_error_message(c));

  }

end:
  return ret;

err:
  ret = 1;
  goto end;

}

static int
nbst_check_tree(
    nbst_t *tree,
    KEY_T *buf) {

  __label__ end, err;
  int ret = 0;
  size_t i;
  nbst_ret_t c;
  KEY_T  k;
#ifdef VALUE_T
  VALUE_T v;
#endif /* VALUE_T */

  for(i=0; i < size; ++i) {

    //printf("i %zd\n", i);
    c = nbst_search(
            tree,
            buf[i],
            &k
#ifdef VALUE_T
           ,&v
#endif /* VALUE_T */
          );

    if ( nbst_unlikely(c != NBST_OK) )
      fail("can't find element at %zd in the tree: %s",
           i, nbst_error_message(c));

    if ( nbst_unlikely(nbst_key_cmp(buf[i], k)) )
      fail("returned key doesn't match at %zd", i);

#ifdef VALUE_T
    if ( v != 0 ) {
      if ( nbst_unlikely(nbst_value_cmp(buf[i], v)) )
        fail("returned value doesn't match at %zd", i);
    }
#endif /* VALUE_T */

  }

end:
  return ret;

err:
  ret = 1;
  goto end;

}

static int
nbst_remove_test(
    nbst_t *tree,
    KEY_T *buf) {

  __label__ end, err;
  int ret = 0;
  size_t step, e, i, j, h;
  nbst_ret_t c;
  KEY_T k;
#ifdef VALUE_T
  VALUE_T v;
#endif /* VALUE_T */

  h = size / 4;

  for(step = 0, e = min(step + h, size);
      step < size;
      step = e, e = min(step + h, size) ) {

    nbst_debug("step %zd", step);

    for(i = step; i < e; ++i) {
      nbst_debug("remove key %zd:", i);
      nbst_key_print(buf[i]);

      c = nbst_remove(tree, buf[i]);
      if ( nbst_unlikely(c != NBST_OK) ) {
        fail("can't remove key at %zd: %s",
             i, nbst_error_message(c));
      }

    }

    for(j = 0; j < i; ++j) {

      nbst_debug("ckeck if key %zd not there", j);
      nbst_key_print(buf[j]);

      c = nbst_search(
              tree,
              buf[j],
              &k
#ifdef VALUE_T
             ,&v
#endif /* VALUE_T */
             );

      if ( nbst_unlikely(c != NBST_NOTFOUND) ) {
        fail("key at %zd should not be found: %s",
             j, nbst_error_message(c));
      }

    }

    for(; j < size; ++j) {

      nbst_debug("check if key %zd is there", j);
      nbst_key_print(buf[j]);

      c = nbst_search(
              tree,
              buf[j],
              &k
#ifdef VALUE_T
             ,&v
#endif /* VALUE_T */
             );

      nbst_debug("got key:");
      nbst_key_print(k);

#ifdef VALUE_T
      nbst_debug("got value:");
      nbst_value_print(v);
#endif /* VALUE_T */

      if ( nbst_unlikely(c != NBST_OK) )
        fail("can't find key at %zd: %s",
             j, nbst_error_message(c));

      if ( nbst_unlikely(nbst_key_cmp(buf[j], k)) )
        fail("wrong key returned at %zd", j);

#ifdef VALUE_T
      if ( nbst_unlikely(v == 0) ) {
        fail("value is null");
      }

      if ( nbst_unlikely(nbst_value_cmp(buf[j], v)) )
        fail("wrong value returned at %zd", j);
#endif /* VALUE_T */

    }

  }

end:
  return ret;

err:
  ret = 1;
  goto end;

}

#undef nbst_t
#undef nbst_insert_test
#undef nbst_check_tree
#undef nbst_search
#undef nbst_insert
#undef nbst_key_cmp
#undef nbst_remove
#undef nbst_remove_check
#undef nbst_key_print
#undef nbst_value_print
#undef KEY_T
#undef VALUE_T
#undef nbst_name
#undef _nbst_name
#undef __nbst_name
#undef nbst_value_cmp
#undef nbst_key_print
#undef nbst_value_print
