/* Copyright (c) 2013-2014 Andrej Gelenberg <andrej.gelenberg@udo.edu> */

#define _DEFAULT_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <fcntl.h>
#include <nbst/nbst_lib.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>
#include <assert.h>

#include "msgs.h"

static int
nbst_cstr_cstr_key_cmp(
    const char *a,
    const char *b);

static int
nbst_cstr_cstr_data_cmp(
    const char *a,
    const char *b)
  __attribute__((alias("nbst_cstr_cstr_key_cmp")));

/* size schould be >3 */
size_t size = 16*1024;
size_t order = 11;

static int
nbst_cstr_cstr_key_cmp(
    const char *a,
    const char *b) {

  return strcmp(a, b);

}

#ifdef DEBUG
static int
nbst_cstr_cstr_key_print(
    const cstr str) {

  fprintf(stderr, "%p - ", str);
  return fprintf(stderr, "%s\n", str);

}

static int
nbst_long_key_print(
    const long i) {

  return fprintf(stderr, "%ld\n", i);

}

static int
nbst_cstr_cstr_value_print(
    const cstr val)
  __attribute__((alias("nbst_cstr_cstr_key_print")));

#endif /* DEBUG */

static int
nbst_cstr_cstr_value_cmp(
    const char *a,
    const char *b) {

  return strcmp(a, b);

}

static unsigned int
get_seed() {

  __label__ fallback, end;

  unsigned int ret;
  int fd;
  size_t c;

  fd = open("/dev/urandom", O_RDONLY);

  if ( nbst_unlikely(fd == -1) ) goto fallback;

  c = read(fd, &ret, sizeof(ret));
  close(fd);

  if ( nbst_unlikely(c != sizeof(ret)) ) goto fallback;

  goto end;

fallback:
  warn("can't init seed from /dev/urandom");
  struct timeval tv;
  gettimeofday(&tv, NULL);
  ret = tv.tv_sec ^ tv.tv_usec;

end:
  info("got seed: %d", ret);
  return ret;

}

static int
nbst_long_key_cmp(
    long a, long b) {

  int ret;
  if ( a == b ) ret = 0;
  else ret = (a > b) ? 1 : -1;
  return ret;

}

struct nbst_long_foreach_data_t {
    size_t len;
    int    last_set;
    long   last;
};
typedef struct nbst_long_foreach_data_t nbst_long_foreach_data_t;

static nbst_foreach_ret_t
check_long_tree(
    long  key,
    void *cbdata) {

  __label__ end, err;
  nbst_long_foreach_data_t *data = cbdata;
  nbst_foreach_ret_t ret = NBST_FOREACH_OK;
  int cmp;

  ++(data->len);
  if ( nbst_likely(data->last_set) ) {
    cmp = nbst_long_key_cmp(key, data->last);
    if ( nbst_unlikely(cmp <= 0) ) {
      fail(
          "tree itegrity fails at key %ld (%ld) * %d cmp: %d",
          key,
          data->last,
          data->last_set,
          cmp);

    }
  } else {

    data->last_set = 1;

  }

  data->last = key;

end:
  return ret;

err:
  ret = NBST_FOREACH_ERROR;
  goto end;
}

#define KEY_T long
#include "test.h"

#define KEY_T   cstr
#define VALUE_T cstr
#include "test.h"

static int
long_tests() {

  __label__ end, err, retry;
  int ret = 0;
  size_t i, j, n;
  long k;
  nbst_long_t *tree = 0;
  nbst_ret_t c;
  nbst_long_foreach_data_t d;

  long *buf = malloc(size * sizeof(buf[0]));
  if ( !buf ) fail("no mem");

  c = nbst_long_new(&tree, order);
  if ( nbst_unlikely(c != NBST_OK) )
    fail("can't create new tree: %s",
         nbst_error_message(c));

  info("sorted test\nfill buffer");
  for(i = 0; i < size; ++i) buf[i] = i + 1;

  info("insert buffer into tree");
  if ( nbst_unlikely(nbst_long_insert_test(tree, buf)) )
    goto err;

  info("verify tree");
  if ( nbst_unlikely(nbst_long_length(tree) != size) )
    fail("tree has wrong size");

  if ( nbst_unlikely(nbst_long_check_tree(tree, buf)) )
    goto err;

  d.len  = 0;
  d.last_set = 0;
  c = nbst_long_foreach(tree, &d, check_long_tree);
  if ( nbst_unlikely(c != NBST_OK) )
    goto err;

  if ( nbst_unlikely(d.len != size) )
    fail("number of elements in the tree is wrong");

  info("remove elements from tree");
  if ( nbst_unlikely(nbst_long_remove_test(tree, buf)) )
    goto err;

  if ( nbst_unlikely(nbst_long_length(tree) != 0) )
    fail("tree is not empty after removing all elements");

  info("reverse sorted remove test\ninsert buffer in the tree");
  if ( nbst_unlikely(nbst_long_insert_test(tree, buf)) )
    goto err;

  info("fill buffer in reverse order");
  for(i = 0; i < size; ++i) buf[i] = size - i;

  info("remove keys from the tree");
  if ( nbst_unlikely(nbst_long_remove_test(tree, buf)) )
    goto err;

  if ( nbst_unlikely(nbst_long_length(tree) != 0) )
    fail("tree is not empty after removing all elements");

  info("reverse sorted test\ninsert buffer into tree");
  if ( nbst_unlikely(nbst_long_insert_test(tree, buf)) )
    goto err;

  info("verify tree");
  if ( nbst_unlikely(nbst_long_length(tree) != size) )
    fail("tree has wrong size");

  if ( nbst_unlikely(nbst_long_check_tree(tree, buf)) )
    goto err;

  d.len  = 0;
  d.last_set = 0;
  c = nbst_long_foreach(tree, &d, check_long_tree);
  if ( nbst_unlikely(c != NBST_OK) )
    goto err;

  if ( nbst_unlikely(d.len != size) )
    fail("number of elements in the tree is wrong");

  info("remove elements from tree");
  if ( nbst_unlikely(nbst_long_remove_test(tree, buf)) )
    goto err;

  if ( nbst_unlikely(nbst_long_length(tree) != 0) )
    fail("tree is not empty after removing all elements");

  info("random sorted test");

  srand(get_seed());

  for(n = 0; n < 4; ++n) {

    info("fill buffer");

    for(i=0; i<size; ++i) {

retry:
#if LONG_BIT <= 32
      k = rand();
#else
      k = (((long)rand())<<32) | (long)rand();
#endif

      for(j=0; j<i; ++j)
        if ( nbst_unlikely((k == 0) || (buf[j] == k)) )
          goto retry;

      buf[i] = k;

    }

    info("insert buffer to the tree");

    if ( nbst_unlikely(nbst_long_insert_test(tree, buf)) )
      goto err;

    info("verify tree");

    if ( nbst_unlikely(nbst_long_length(tree) != size) )
      fail("tree has wrong size");

    if ( nbst_unlikely(nbst_long_check_tree(tree, buf)) )
      goto err;

    d.len = 0;
    d.last_set = 0;
    c = nbst_long_foreach(tree, &d, check_long_tree);
    if ( nbst_unlikely(c != NBST_OK) )
      goto err;

    if ( nbst_unlikely(d.len != size) )
      fail("number of elements in the tree is wrong");

    info("remove elements from tree");
    if ( nbst_unlikely(nbst_long_remove_test(tree, buf)) )
      goto err;

    if ( nbst_unlikely(nbst_long_length(tree) != 0) )
      fail("tree is not empty after removing all elements");

  }

end:
  if ( nbst_likely(buf  != 0) ) free(buf);
  if ( nbst_likely(tree != 0) ) nbst_long_free(tree);
  return ret;

err:
  ret = 1;
  goto end;
}

static void
clear_str_buf(
    char **buf) {

  size_t i;

  for(i = 0; i < size; ++i) {

    if ( nbst_likely(buf[i] != 0) )
      free(buf[i]);

  }

  free(buf);

}

static size_t
trav_dirs(
    size_t idx,
    const char *dir,
    size_t dlen,
    char **retbuf) {

  DIR *d = 0;
  struct dirent *ent;
  char *buf = malloc(PATH_MAX);
  char *ibuf;
  size_t len, blen;
  int c;
  struct stat s;

  if ( nbst_unlikely(!buf) ) {
    error("no mem");
    goto end;
  }

  d = opendir(dir);
  if ( nbst_unlikely(!d) )
    goto end;

  memcpy(buf, dir, dlen);
  ibuf = &(buf[dlen]);

  while ( nbst_likely((ent = readdir(d)) != 0) ) {

    if ( (ent->d_name[0] == '.') &&
         ( (ent->d_name[1] == 0) ||
           ((ent->d_name[1] == '.') && (ent->d_name[2] == 0))))
      continue;

    len = strlen(ent->d_name);
    blen = dlen + len;

    if ( nbst_unlikely(blen >= (PATH_MAX - 1)) )
      continue;

    memcpy(ibuf, ent->d_name, len+1);

    c = stat(buf, &s);

    if ( nbst_likely(c == 0) && S_ISDIR(s.st_mode) ) {
      buf[blen] = '/';
      ++blen;
      buf[blen] = 0;

      idx = trav_dirs(idx, buf, blen, retbuf);
      if ( idx == size ) break;
    }

    retbuf[idx] = malloc(blen+1);

    if ( nbst_unlikely(!retbuf[idx]) ) {
      error("no mem");
      goto end;
    }

    memcpy(retbuf[idx], buf, blen+1);

    ++idx;

    if ( idx == size ) break;

  }

end:
  if ( nbst_likely(d != 0) )
    closedir(d);

  if ( nbst_likely(buf != 0) )
    free(buf);

  return idx;

}


static char **
get_str_buf() {

  __label__ end, err;

  char **ret = calloc(size, sizeof(ret[0]));
  if ( nbst_unlikely(!ret) ) {
    error("no mem");
    goto end;
  }

  info("travel the fs for strings");

  size_t i = trav_dirs(0, "/", sizeof("/") - 1, ret);
  if ( nbst_unlikely(i != size) )
    fail("not enouth string were found (%zd < %zd)", i, size);

end:
  return ret;

err:
  free(ret);
  ret = 0;
  goto end;

}

static int
cstr_cstr_tests() {

  __label__ end, err;
  int ret = 0;

  info("fill buffer");

  nbst_cstr_cstr_t *tree = 0;
  char **buf = get_str_buf();
  if ( nbst_unlikely(buf == 0) )
    goto err;

  if ( nbst_unlikely(nbst_cstr_cstr_new(&tree, order) != NBST_OK) ) {
    error("no mem");
    goto err;
  }

  info("insert buffer into tree");

  if ( nbst_unlikely(nbst_cstr_cstr_insert_test(tree, (const char **)buf)) )
    goto err;

  info("verify tree");

  if ( nbst_unlikely(nbst_cstr_cstr_length(tree) != size) )
    fail("tree has wrong size");

  if ( nbst_unlikely(nbst_cstr_cstr_check_tree(tree, (const char **)buf)) )
    goto err;

  info("test remove");

  if ( nbst_unlikely(nbst_cstr_cstr_remove_test(tree, (const char **)buf)) )
    goto err;

  if ( nbst_unlikely(nbst_cstr_cstr_length(tree) != 0) )
    fail("tree length not 0 after removing all elements");

  nbst_cstr_cstr_free(tree);

  info("test freeing full tree (run test with valgrind to see results)");

  if ( nbst_unlikely(nbst_cstr_cstr_new(&tree, order) != NBST_OK) ) {
    error("no mem");
    goto err;
  }

  info("insert buffer into tree");

  if ( nbst_unlikely(nbst_cstr_cstr_insert_test(tree, (const char **)buf)) )
    goto err;

  info("cleanup");


end:
  if ( nbst_likely(buf != 0) )
    clear_str_buf(buf);
  if ( nbst_likely(tree != 0) )
    nbst_cstr_cstr_free(tree);
  return ret;

err:
  ret = 1;
  goto end;

}

int
main(
    int argc,
    char **argv) {

  __label__ end, err;
  int ret = 0;

#define try(cmd) { \
    info("run test: " # cmd); \
    if ( nbst_unlikely(cmd) ) { \
        error("test failed: " # cmd); \
        goto err; } \
    pass("test pass: " # cmd); \
  }

  try(long_tests());
  try(cstr_cstr_tests());

#undef try

end:
  return ret;

err:
  ret = 1;
  goto end;

}
